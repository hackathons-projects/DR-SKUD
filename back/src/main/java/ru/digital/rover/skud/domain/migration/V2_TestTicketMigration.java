package ru.digital.rover.skud.domain.migration;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.digital.rover.skud.domain.model.*;
import ru.digital.rover.skud.domain.repository.TicketRepository;
import ru.digital.rover.skud.domain.repository.UserRepository;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

@RequiredArgsConstructor
@Transactional
@Component
public class V2_TestTicketMigration implements Migration {

    private final TicketRepository ticketRepository;
    private final UserRepository userRepository;

    @Override
    public String getId() {
        return V2_TestTicketMigration.class.getName();
    }

    @Override
    public void migrate() {
        Set<AuthType> authTypes1 = new HashSet<>(Arrays.asList(AuthType.QR, AuthType.NFC));
        Set<GraphNodeEntity> graphNodes1 = new HashSet<>();
        ticketRepository.save(new TicketEntity(graphNodes1,
                "admin@dr.ru", "Admin", "+7..",
                userRepository.findByEmail("admin@dr.ru").orElseThrow(),
                LocalDate.now(), LocalDate.now(),
                TicketStatus.APPROVED, TicketType.REGULAR, authTypes1));

        IntStream.range(1, 100).forEach(index -> {
            Set<AuthType> authTypes = new HashSet<>(Arrays.asList(AuthType.QR, AuthType.NFC));
            Set<GraphNodeEntity> graphNodes = new HashSet<>();
            ticketRepository.save(new TicketEntity(graphNodes,
                    "user" + index + "@dr.ru", "User" + index, "+7..",
                    userRepository.findByEmail("user" + index + "@dr.ru").orElseThrow(),
                    LocalDate.now(), LocalDate.now(),
                    index % 2 == 0 ? TicketStatus.APPROVED : TicketStatus.REQUEST,
                    index % 2 == 0 ? TicketType.REGULAR : TicketType.TEMPORARY,
                    authTypes));
        });
    }
}
