package ru.digital.rover.skud.domain.model;

public enum Role {
    USER, ADMIN, SERVICE, GUEST
}
