package ru.digital.rover.skud.domain.migration;


import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.digital.rover.skud.domain.model.Gender;
import ru.digital.rover.skud.domain.model.Role;
import ru.digital.rover.skud.domain.model.UserEntity;
import ru.digital.rover.skud.domain.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.stream.IntStream;

@RequiredArgsConstructor
@Transactional
@Component
public class V1_UserMigration implements Migration {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public String getId() {
        return V1_UserMigration.class.getName();
    }

    @Override
    public void migrate() {
        userRepository.save(new UserEntity(
                "admin@dr.ru",
                passwordEncoder.encode("admin"),
                Role.ADMIN,
                "Admin",
                "TECH",
                LocalDateTime.now(),
                Gender.NONE,
                "XXXXXXXXXX",
                "Moscow"
        ));
        IntStream.range(1, 100).forEach(index -> {
            userRepository.save(new UserEntity(
                    "user" + index + "@dr.ru",
                    passwordEncoder.encode("user"),
                    index % 2 == 0 ? Role.USER : Role.GUEST,
                    "User" + index,
                    "TECH",
                    LocalDateTime.now(),
                    index % 2 == 0 ? Gender.MALE : Gender.FEMALE,
                    "XXXXXXXXXX",
                    "Moscow"
            ));
        });
    }
}
