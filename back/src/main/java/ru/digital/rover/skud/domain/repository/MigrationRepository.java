package ru.digital.rover.skud.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.digital.rover.skud.domain.model.MigrationEntity;

public interface MigrationRepository extends JpaRepository<MigrationEntity, Long> {

    boolean existsByClassName(String className);
}
