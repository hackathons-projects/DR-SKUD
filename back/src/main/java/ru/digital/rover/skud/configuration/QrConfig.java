package ru.digital.rover.skud.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Setter
@Getter
@ConfigurationProperties(prefix = "qr.size")
public class QrConfig {
    private Integer x;
    private Integer y;
}
