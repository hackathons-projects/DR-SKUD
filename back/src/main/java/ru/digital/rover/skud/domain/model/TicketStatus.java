package ru.digital.rover.skud.domain.model;

public enum TicketStatus {
    REQUEST, APPROVED, REVOKED, EXPIRED
}
