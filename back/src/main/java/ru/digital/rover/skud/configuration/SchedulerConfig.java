package ru.digital.rover.skud.configuration;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@AllArgsConstructor
@Configuration
@EnableScheduling
public class SchedulerConfig {

}
