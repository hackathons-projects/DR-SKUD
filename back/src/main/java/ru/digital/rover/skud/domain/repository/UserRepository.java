package ru.digital.rover.skud.domain.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.digital.rover.skud.domain.model.UserEntity;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {

    Optional<UserEntity> findByEmail(String email);

    List<UserEntity> findAll();

}
