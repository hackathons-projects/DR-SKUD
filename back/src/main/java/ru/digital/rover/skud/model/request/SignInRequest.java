package ru.digital.rover.skud.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.digital.rover.skud.configuration.security.GrantType;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SignInRequest {

    private String email;

    private String password;

    private String token;

    private String refreshToken;

    private GrantType grantType;
}
