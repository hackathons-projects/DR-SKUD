package ru.digital.rover.skud.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Audited
@Entity
@Table(name = "skud_users")
public class UserEntity extends BaseEntity<Long> {

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(nullable = false, length = 100)
    private String name;

    @Column(length = 1000)
    private String organization;

    @Column(nullable = false)
    private LocalDateTime birthDay;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column
    private String passportNumber;

    @Column
    private String city;

}

