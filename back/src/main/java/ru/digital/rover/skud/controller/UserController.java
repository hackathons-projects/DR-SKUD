package ru.digital.rover.skud.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.digital.rover.skud.domain.model.Role;
import ru.digital.rover.skud.model.UserDto;
import ru.digital.rover.skud.service.UserService;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

//@PreAuthorize("isAuthenticated()")
@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping
    public ResponseEntity<Page<UserDto>> getAllUsers(Pageable pageable) {
        return ResponseEntity.ok(userService.getAllUsers(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable Long id) {
        return ResponseEntity.ok(userService.getUser(id));
    }

    @PutMapping("/{id}/password")
    public ResponseEntity<UserDto> changeUserPassword(@PathVariable Long id,
                                                      @Valid @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$") String password) {
        return ResponseEntity.ok(userService.updatePassword(id, password));
    }

    /**
     * Make user an admin
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/{id}/make-admin")
    public ResponseEntity<UserDto> makeUserAdmin(@PathVariable Long id) {
        return ResponseEntity.ok(userService.changeRole(id, Role.ADMIN));
    }

    /**
     * Remove from admins user
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/{id}/remove-admin")
    public ResponseEntity<UserDto> removeUserAdminRole(@PathVariable Long id) {
        return ResponseEntity.ok(userService.changeRole(id, Role.USER));
    }
}
