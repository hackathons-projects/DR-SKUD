package ru.digital.rover.skud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@ConfigurationPropertiesScan("ru.digital.rover")
@SpringBootApplication
public class SkudApplication {

    public static void main(String[] args) {
        SpringApplication.run(SkudApplication.class, args);
    }

}
