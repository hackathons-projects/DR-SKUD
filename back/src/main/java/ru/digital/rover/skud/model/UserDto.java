package ru.digital.rover.skud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.digital.rover.skud.domain.model.Gender;
import ru.digital.rover.skud.domain.model.Role;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserDto {

    private Long id;

    private String email;

    private String password;

    private Role role;

    private String name;

    private String organization;

    private String birthDay;

    private Gender gender;

    private String passportNumber;

    private String city;
}
