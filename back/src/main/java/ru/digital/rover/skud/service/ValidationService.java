package ru.digital.rover.skud.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.digital.rover.skud.exception.CustomException;
import ru.digital.rover.skud.model.request.BuyTicketRequest;

import java.time.LocalDate;

@Slf4j
@RequiredArgsConstructor
@Transactional
@Service
public class ValidationService {

    private final ModelMapper modelMapper;
    private final AuthService authService;


    public void validateAdminOrOwner(Long id) {
        if (!(authService.isAdmin() && authService.isOwner(id))) {
            throw new CustomException("Access denied", HttpStatus.FORBIDDEN);
        }
    }

    public void validateTicketDates(BuyTicketRequest ticketRequest) {
        if (modelMapper.map(ticketRequest.getStartTripDate(), LocalDate.class).isAfter(modelMapper.map(ticketRequest.getEndTripDate(), LocalDate.class))) {
            throw new CustomException("Дата начала должна быть перед датой завершения", HttpStatus.BAD_REQUEST);
        } else if (modelMapper.map(ticketRequest.getStartTripDate(), LocalDate.class).isBefore(LocalDate.now())
                || modelMapper.map(ticketRequest.getEndTripDate(), LocalDate.class).isBefore(LocalDate.now())) {
            throw new CustomException("Дата начала и завершения не должны быть из прошлого", HttpStatus.BAD_REQUEST);
        }
    }
}
