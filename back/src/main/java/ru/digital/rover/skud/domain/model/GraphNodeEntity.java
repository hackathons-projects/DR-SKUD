package ru.digital.rover.skud.domain.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
@Entity
@Table(name = "checkpoints")
public class GraphNodeEntity extends BaseEntity<Long> {

    @Column
    private String name;

    @Column
    private String description;

    @OneToMany(mappedBy = "destination", cascade = CascadeType.ALL)
    private Set<GraphNodeLinkEntity> path = new HashSet<>();
}
