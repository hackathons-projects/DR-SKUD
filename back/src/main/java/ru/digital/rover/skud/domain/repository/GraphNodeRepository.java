package ru.digital.rover.skud.domain.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.digital.rover.skud.domain.model.GraphNodeEntity;

import java.util.Collection;
import java.util.Set;

public interface GraphNodeRepository extends PagingAndSortingRepository<GraphNodeEntity, Long> {

    Set<GraphNodeEntity> findAllByIdIn(Collection<Long> id);
}
