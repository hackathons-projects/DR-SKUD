package ru.digital.rover.skud.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.digital.rover.skud.domain.model.TicketEntity;
import ru.digital.rover.skud.exception.CustomException;
import ru.digital.rover.skud.model.QrTicketDto;
import ru.digital.rover.skud.model.TicketDto;
import ru.digital.rover.skud.service.CryptoService;

import java.nio.charset.StandardCharsets;
import java.security.Signature;
import java.security.SignatureException;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Optional;
import java.util.stream.Collectors;

@Transactional
@Component
public class QrCodeConverter implements Converter<TicketEntity, QrTicketDto> {

    private final DateTimeFormatter FORMATTER;
    private final CryptoService cryptoService;
    private final ObjectMapper objectMapper;

    public QrCodeConverter(@Value("${format.local-date}") String format, CryptoService cryptoService, ObjectMapper objectMapper) {
        this.FORMATTER = DateTimeFormatter.ofPattern(format);
        this.cryptoService = cryptoService;
        this.objectMapper = objectMapper;
    }

    @Override
    public QrTicketDto convert(MappingContext<TicketEntity, QrTicketDto> mappingContext) {
        QrTicketDto result = Optional.ofNullable(mappingContext.getDestination()).orElse(new QrTicketDto());
        TicketDto ticketDto = new TicketDto();
        ticketDto.setId(mappingContext.getSource().getId());
        ticketDto.setCheckPointIds(mappingContext.getSource().getCheckPoints().stream().map(checkPointEntity -> checkPointEntity.getId()).collect(Collectors.toSet()));
        ticketDto.setEmail(mappingContext.getSource().getEmail());
        ticketDto.setName(mappingContext.getSource().getName());
        ticketDto.setPhone(mappingContext.getSource().getPhone());
        ticketDto.setActivationDate(mappingContext.getSource().getActivationDate().format(FORMATTER));
        ticketDto.setExpirationDate(mappingContext.getSource().getExpirationDate().format(FORMATTER));
        ticketDto.setType(mappingContext.getSource().getType());
        ticketDto.setStatus(mappingContext.getSource().getStatus());
        ticketDto.setAuthTypes(mappingContext.getSource().getAuthTypes());
        result.setTicketDto(ticketDto);

        try {
            Signature signatureForSign = cryptoService.getSignatureForSign();
            signatureForSign.update(objectMapper.writeValueAsString(ticketDto).getBytes(StandardCharsets.UTF_8));
            result.setSignature(Base64.getEncoder().encodeToString(signatureForSign.sign()));
        } catch (JsonProcessingException | SignatureException e) {
            throw new CustomException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return result;
    }
}
