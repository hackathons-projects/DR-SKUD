package ru.digital.rover.skud.domain.model;

import lombok.*;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Audited
@EqualsAndHashCode(callSuper = true, exclude = {"checkPoints", "user"})
@ToString(exclude = {"checkPoints", "user"})
@Entity
@Table(name = "tickets")
public class TicketEntity extends BaseEntity<Long> {

    @NotAudited
    @ManyToMany
    private Set<GraphNodeEntity> checkPoints = new HashSet<>();

    @Column
    private String email;

    @Column
    private String name;

    @Column
    private String phone;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;

    @Column
    private LocalDate activationDate;

    @Column
    private LocalDate expirationDate;

    @Enumerated(EnumType.STRING)
    private TicketStatus status;

    @Enumerated(EnumType.STRING)
    private TicketType type;

    @ElementCollection
    private Set<AuthType> authTypes;

}
