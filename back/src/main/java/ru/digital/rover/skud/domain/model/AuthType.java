package ru.digital.rover.skud.domain.model;

public enum AuthType {
    QR, BLE, NFC, FACE, VOICE
}
