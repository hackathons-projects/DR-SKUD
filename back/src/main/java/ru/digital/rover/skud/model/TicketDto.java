package ru.digital.rover.skud.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.digital.rover.skud.domain.model.AuthType;
import ru.digital.rover.skud.domain.model.GraphNodeEntity;
import ru.digital.rover.skud.domain.model.TicketStatus;
import ru.digital.rover.skud.domain.model.TicketType;

import java.util.HashSet;
import java.util.Set;

@JsonPropertyOrder(alphabetic=true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class TicketDto {

    private Long id;

    private Set<Long> checkPointIds = new HashSet<>();

    private String email;

    private String name;

    private String phone;

    private String activationDate;

    private String expirationDate;

    private TicketStatus status;

    private TicketType type;

    private Set<AuthType> authTypes;

}
