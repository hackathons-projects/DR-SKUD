package ru.digital.rover.skud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GraphNodeDto {

    private String name;

    private String description;

    private Set<Long> path = new HashSet<>();

}
