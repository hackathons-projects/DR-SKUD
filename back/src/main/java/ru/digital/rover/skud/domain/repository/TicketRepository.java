package ru.digital.rover.skud.domain.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.digital.rover.skud.domain.model.TicketEntity;

import java.util.Set;

public interface TicketRepository extends PagingAndSortingRepository<TicketEntity, Long> {
}
