package ru.digital.rover.skud.domain.model;

public enum TicketType {
    TEMPORARY, REGULAR, DISPOSABLE
}
