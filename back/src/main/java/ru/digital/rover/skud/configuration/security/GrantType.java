package ru.digital.rover.skud.configuration.security;

public enum GrantType {
    PASSWORD, REFRESH_TOKEN
}
