package ru.digital.rover.skud.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Setter
@Getter
@ConfigurationProperties(prefix = "crypto")
public class CryptoConfig {
    private AesConfig aes;
    private RsaConfig rsa;
    private HybridRsaAesKey hybridAesRsaKey;
    private Signature signature;

    @Setter
    @Getter
    public static class AesConfig {
        private String algorithm;
        private String transformation;
        private String keyBase64;
    }

    @Setter
    @Getter
    public static class RsaConfig {
        private String algorithm;
        private String transformation;
        private String publicKey;
        private String privateKey;
    }

    @Setter
    @Getter
    public static class HybridRsaAesKey {
        private String key;
    }

    @Setter
    @Getter
    public static class Signature {
        private String algorithm;
    }
}
