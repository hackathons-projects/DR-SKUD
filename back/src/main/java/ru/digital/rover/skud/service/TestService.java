package ru.digital.rover.skud.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.digital.rover.skud.model.QrTicketDto;
import ru.digital.rover.skud.model.TicketDto;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.nio.charset.StandardCharsets;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Base64;

@Slf4j
@Transactional
@RequiredArgsConstructor
@Service
public class TestService {

    private final CryptoService cryptoService;
    private final ObjectMapper objectMapper;
    private final TicketService ticketService;

    @PostConstruct
    private void test() throws Exception {
        testAes();
        testRsa();
        testAes_RsaKey();
        testVerification();
        extTestVerification();
        st();
    }

    private void testAes() throws Exception {
        String src = "Тестовая строка TEST";
        byte[] bbb = cryptoService.getEncryptAesCipher().doFinal(src.getBytes(StandardCharsets.UTF_8));
        System.out.println(new String(bbb));
        System.out.println(new String(Base64.getEncoder().encode(bbb)));


        byte[] rrr = cryptoService.getDecryptAesCipher().doFinal(bbb);
        System.out.println(new String(rrr));
        System.out.println();
    }

    private void testRsa() throws BadPaddingException, IllegalBlockSizeException {
        String myString = "Моя строка";
        byte[] bytes = cryptoService.getEncryptRsaCipher().doFinal(myString.getBytes(StandardCharsets.UTF_8));
        System.out.println(new String(bytes));

        byte[] res = cryptoService.getDecryptRsaCipher().doFinal(bytes);
        System.out.println(new String(res));
        System.out.println();
    }

    private void testAes_RsaKey() throws BadPaddingException, IllegalBlockSizeException {
        String s = "Э-э-эх";
        byte[] bbb = cryptoService.getEncryptHybridCipher().doFinal(s.getBytes(StandardCharsets.UTF_8));
        System.out.println(new String(bbb));


        byte[] rrr = cryptoService.getDecryptHybridCipher().doFinal(bbb);
        System.out.println(new String(rrr));
        System.out.println();
    }

    private void testVerification() {
        try {
            Signature signatureForSign = cryptoService.getSignatureForSign();
            signatureForSign.update("test".getBytes(StandardCharsets.UTF_8));
            byte[] sign = signatureForSign.sign();

            Signature signatureForVerify = cryptoService.getSignatureForVerify();
            signatureForVerify.update("test".getBytes(StandardCharsets.UTF_8));
            boolean verify = signatureForVerify.verify(sign);
            System.out.println(verify);
            System.out.println();

        } catch (SignatureException e) {
            e.printStackTrace();
        }
    }

    private void extTestVerification() {
        try {
            String dto = "{\"activationDate\":\"20-03-2021\",\"authTypes\":[\"NFC\",\"QR\"],\"checkPointIds\":[],\"email\":\"admin@dr.ru\",\"expirationDate\":\"20-03-2021\",\"id\":1,\"name\":\"Admin\",\"phone\":\"+7..\",\"status\":\"APPROVED\",\"type\":\"REGULAR\"}";

            Signature signatureForSign = cryptoService.getSignatureForSign();
            signatureForSign.update(dto.getBytes(StandardCharsets.UTF_8));
            byte[] sign = signatureForSign.sign();
            String old = Base64.getEncoder().encodeToString(sign);
            byte[] ss = Base64.getDecoder().decode("tqYvuJ8O1M0EuEXTFdDqS7YdchOsouEV5yG8NWGGj9dAx1M/uXcQ+3xnBDtb95YqYcy8EmreAwAf274jEAcsJ2GdYWzKi0i8+FR/UftePErRSbNGE/MhveA7ZUOEVXgFyWukdWmLfFBaUmZTHfrm8xFTUlgFqsco60u5eMf20J+5Q7Xyt3W7Fxb9AzJ1K9iLvS3Q4lJABr5+tsyHCbrcfP5az8G7caQg5aktSfmaMNx6ObwlMcTiaKrn+Eq3WKr8rf5gS6ijELpSbuAjqU9IjnN51vWaFtzSW21gPUqGQE7n8CkwDrNafvl2x0ZoFAZ+0XCZFrMlaX6xktLm1TeKkw==");

            String dto2 = "{\"activationDate\":\"20-03-2021\",\"authTypes\":[\"NFC\",\"QR\"],\"checkPointIds\":[],\"email\":\"admin@dr.ru\",\"expirationDate\":\"20-03-2021\",\"id\":1,\"name\":\"Admin\",\"phone\":\"+7..\",\"status\":\"APPROVED\",\"type\":\"REGULAR\"}";
            Signature signatureForVerify = cryptoService.getSignatureForVerify();
            signatureForVerify.update(dto2.getBytes(StandardCharsets.UTF_8));
            boolean verify = signatureForVerify.verify(ss);
            System.out.println(verify);
            System.out.println();

        } catch (SignatureException e) {
            e.printStackTrace();
        }
    }

    private void st(){
        String input = "{\"ticketDto\":{\"activationDate\":\"20-03-2021\",\"authTypes\":[\"QR\",\"NFC\"],\"checkPointIds\":[],\"email\":\"admin@dr.ru\",\"expirationDate\":\"20-03-2021\",\"id\":1,\"name\":\"Admin\",\"phone\":\"+7..\",\"status\":\"APPROVED\",\"type\":\"REGULAR\"},\"signature\":\"fgnjOfxn1dmFUo0o2Wozzxs8phcRtw/MqeYP6jwIOVl6ioShrIUdtMRLAIsI/N4s4nTKTgQhrsQtk7y2skl0AVMuw02XwE4yCgXsI03LOkbffIaUiH5dL/xpby/Flp/1mRteS6UcZRdUJCPKTgdYGtD8DdGhzLC6nDcLv8J8pug2eJP6YWCrtW/jXKsPwWlB/WMVppg1b9BT6LezVE96oAsWb1RZDcERo/McKgfdqEM7wA7ktkJJujrI4N+x98agJLwqk+i7beVTHUPBhvCWDA0U+lnncG7eowLj/0jh5mS8nZg7VVoVro73eSLv5xcldmFhOBirAvIwN2T3Ev3z+A==\"}";
        try {
            QrTicketDto qrTicketDto1 = ticketService.getTicketById(1L);

            /*
            Signature signatureForVerify = cryptoService.getSignatureForVerify();
            signatureForVerify.update(objectMapper.writeValueAsString(ticketDto).getBytes(StandardCharsets.UTF_8));
            signatureForVerify.verify(Base64.getDecoder().decode(result.getSignature()))
            */

            QrTicketDto qrTicketDto = objectMapper.readValue(input, QrTicketDto.class);
            TicketDto ticketDto = qrTicketDto.getTicketDto();
            String dto = objectMapper.writeValueAsString(ticketDto);
            String sig = qrTicketDto.getSignature();

            byte[] ss = Base64.getDecoder().decode(sig);

//"{\"id\":1,\"checkPointIds\":[],\"email\":\"admin@dr.ru\",\"name\":\"Admin\",\"phone\":\"+7..\",\"activationDate\":\"20-03-2021\",\"expirationDate\":\"20-03-2021\",\"status\":\"APPROVED\",\"type\":\"REGULAR\",\"authTypes\":[\"NFC\",\"QR\"]}"

            Signature signatureForVerify = cryptoService.getSignatureForVerify();
            signatureForVerify.update(dto.getBytes(StandardCharsets.UTF_8));
            boolean verify = signatureForVerify.verify(ss);
            System.out.println(verify);
            System.out.println();
        } catch (JsonProcessingException | SignatureException e) {
            e.printStackTrace();
        }
    }

}
