package ru.digital.rover.skud.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.digital.rover.skud.domain.model.AuthType;
import ru.digital.rover.skud.domain.model.TicketType;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BuyTicketRequest {

    @NotNull
    private List<Long> checkPointIds;

    private String email;

    private String name;

    private Integer quantity;

    private String phone;

    private String startTripDate;

    private String endTripDate;

    private TicketType type;

    private Set<AuthType> authTypes;
}
