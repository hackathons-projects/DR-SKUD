package ru.digital.rover.skud.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.digital.rover.skud.model.request.SignInRequest;
import ru.digital.rover.skud.model.response.SignInResponse;
import ru.digital.rover.skud.service.AuthService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/")
public class AuthController {

    private final AuthService authService;

    /**
     * Login a user with a username (email) and password.
     * Find em', check em'.
     * Pass them an authentication token on success.
     * Otherwise, 401. You fucked up.
     *
     * @param signInRequest - {@link SignInRequest} auth request
     * @return {@link SignInResponse}
     */
    @PreAuthorize("permitAll()")
    @PostMapping("/auth")
    public ResponseEntity<SignInResponse> login(@RequestBody SignInRequest signInRequest) {
        return ResponseEntity.ok(authService.login(signInRequest));
    }

    /**
     * Register a user with a username (email) and password.
     * If it already exists, then don't register, duh.
     * <p>
     * body {
     * email: email,
     * password: password
     * }
     */
    @PostMapping("/register")
    public ResponseEntity<?> register() {
        return ResponseEntity.ok().build();
    }

    @PostMapping("/reset")
    public ResponseEntity<?> reset() {
        return ResponseEntity.ok().build();
    }

    /**
     * Reset user's password.
     * {
     * token: STRING
     * password: STRING,
     * }
     */
    @PostMapping("/reset/password")
    public ResponseEntity<?> resetPassword() {
        return ResponseEntity.ok().build();
    }

    /**
     * Resend a password verification email for this user.
     * <p>
     * body {
     * id: user id
     * }
     */
    @PostMapping("/verify/resend")
    public ResponseEntity<?> resendVerify() {
        return ResponseEntity.ok().build();
    }

    @PostMapping("/verify/{token}")
    public ResponseEntity<?> verifyToken(@PathVariable String token) {
        return ResponseEntity.ok().build();
    }
}
