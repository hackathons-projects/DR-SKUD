package ru.digital.rover.skud.domain.model;

public enum Gender {
    MALE, FEMALE, NONE
}
