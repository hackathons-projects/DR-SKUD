package ru.digital.rover.skud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class QrTicketDto {

    private TicketDto ticketDto;

    private String signature;
}
