package ru.digital.rover.skud.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.digital.rover.skud.service.PhotoStorageService;

@PreAuthorize("isAuthenticated()")
@RequiredArgsConstructor
@RestController
@RequestMapping("/photos")
public class PhotoController {

    private final PhotoStorageService photoStorageService;

    @GetMapping("/{photo:.+}")
    public ResponseEntity<Resource> getById(@PathVariable("photo") String photo) {
        Resource resource = photoStorageService.loadFileAsResource(photo);
        return ResponseEntity.ok(resource);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @PostMapping
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) {
        String fileName = photoStorageService.storeFile(file);
        return ResponseEntity.ok(fileName);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @DeleteMapping("/{photo:.+}")
    public ResponseEntity<Void> deleteFile(@PathVariable("photo") String photo) {
        photoStorageService.delete(photo);
        return ResponseEntity.ok().build();
    }

}
