import 'package:cat_analysis_ticket/service/cryptoService.dart';
import 'package:flutter/material.dart';
import 'package:qrcode/qrcode.dart';

import 'dart:async';

import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with TickerProviderStateMixin {
  QRCaptureController _captureController = QRCaptureController();
  Animation<Alignment> _animation;
  AnimationController _animationController;
  static const platForm = const MethodChannel('crypto.channel');

  bool _isTorchOn = false;
  bool _isQrVisible = true;
  String _captureText = '';
  var theMessage = '';

  Future<void> communicate(String value) async {
    String response = "";
    setState(() {
      theMessage = "Invoking";
    });
    try{
      final String result = await platForm.invokeMethod('verify', value);
      response = result;
      setState(() {
        theMessage = response;
        _isQrVisible = false;
        _captureText = theMessage;
      });
    }on PlatformException catch(e){
      setState(() {
        theMessage = "Invocation Error: "+e.message;
      });
    }
  }

  @override
  void initState() {
    super.initState();

    _captureController.onCapture((data) {
      print('onCapture----$data');
      setState(() {
        communicate(data);
      });
    });

    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animation =
    AlignmentTween(begin: Alignment.topCenter, end: Alignment.bottomCenter)
        .animate(_animationController)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _animationController.reverse();
        } else if (status == AnimationStatus.dismissed) {
          _animationController.forward();
        }
      });
    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Охранник'),
        ),
        body: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Visibility(
              child: Container(
                width: 250,
                height: 250,
                child: QRCaptureView(
                  controller: _captureController,
                ),
              ),
              visible: _isQrVisible,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 56),
              child: AspectRatio(
                aspectRatio: 264 / 258.0,
                child: Stack(
                  alignment: _animation.value,
                  children: <Widget>[
                    // Image.asset('images/sao@3x.png'),
                    // Image.asset('images/tiao@3x.png')
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: _buildToolBar(),
            ),
            TextButton(
              onPressed: () {
                _isQrVisible = true;
                _captureText = '';
              },
              child: Text('$_captureText'),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildToolBar() {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        TextButton(
          onPressed: () {
            _captureController.pause();
          },
          child: Text('pause'),
        ),
        TextButton(
          onPressed: () {
            if (_isTorchOn) {
              _captureController.torchMode = CaptureTorchMode.off;
            } else {
              _captureController.torchMode = CaptureTorchMode.on;
            }
            _isTorchOn = !_isTorchOn;
          },
          child: Text('torch'),
        ),
        TextButton(
          onPressed: () {
            _captureController.resume();
          },
          child: Text('resume'),
        ),
      ],
    );
  }
}
