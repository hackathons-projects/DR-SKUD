import 'dart:convert';

import 'package:encrypt/encrypt.dart';

class CryptoService {


  final parser = RSAKeyParser();

   final encrypter = Encrypter(AES(
      Key.fromBase64('vlM8BvMLT6GFV2NJK0lei4rLoXXQsodoNKzP1SjK2t8='),
      mode: AESMode.ecb));

  Future<String> encode(String input) async {
    var qrTicket = encrypter.decrypt(Key.fromBase64(input));
    Map qrMap = json.decode(qrTicket);
    var signature = qrMap['signature'];

    // TODO flutter rsa decrypt
    // final decryptedText = await decryptString(
    //     signature, utf8.decode(base64.decode(rsaKeyBase64)));
    //
    // qrMap['signature'] =
    //     qrMap['startTripDate'] + '/' + 'endTripDate' == decryptedText
    //         ? 'OK'
    //         : 'FAILURE';
    qrMap['signature'] = 'OK';

    return json.encode(qrMap);
  }


}
