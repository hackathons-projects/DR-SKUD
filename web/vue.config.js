// vue.config.js
module.exports = {
  devServer: {
    compress: true,
    disableHostCheck: true,
    inline: true,
    port: '8088'
  }
};
