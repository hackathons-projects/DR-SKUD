import axios from 'axios';
import config from '../config';

class UserService {

  renderRole(row) {
    return row.role === 'ADMIN' ? 'Обслуживающий персонал' : 'Гость';
  }

  renderGender(row) {
    switch (row.gender) {
      case 'MALE':
        return 'Муж';
      case 'FEMALE':
        return 'Жен';
      case 'NONE':
        return 'Не указан';
    }
  }

  getHeaders() {
    return [ // Array of objects
      {
        label: 'Id', // Column name
        field: 'id', // Field name from row
        // Use dot for nested props
        // Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: false    // Escapes output if false.
      },
      {
        label: 'ФИО', // Column name
        field: 'name', // Field name from row
                       // Use dot for nested props
                       // Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: false    // Escapes output if false.
      },
      {
        label: 'Пол', // Column name
        field: row => this.renderGender(row), // Field name from row
        // Use dot for nested props
        // Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: false    // Escapes output if false.
      },
      {
        label: 'Email', // Column name
        field: 'email', // Field name from row
        // Use dot for nested props
        // Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: false    // Escapes output if false.
      },
      {
        label: 'Номер паспорта', // Column name
        field: 'passportNumber', // Field name from row
        // Use dot for nested props
        // Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: false    // Escapes output if false.
      },
      {
        label: 'Дата рождения', // Column name
        field: 'birthDay',
        // Field name from row
        // Use dot for nested props
        // Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: false    // Escapes output if false.
      },
      {
        label: 'Посетитель', // Column name
        field: (row) => {
          return this.renderRole(row);
        }, // Field name from row
        // Use dot for nested props
        // Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: false    // Escapes output if false.
      },
      {
        label: 'Организация', // Column name
        field: 'organization',
        // Field name from row
        // Use dot for nested props
        // Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: false    // Escapes output if false.
      },
      {
        label: 'Город', // Column name
        field: 'city',
        // Field name from row
        // Use dot for nested props
        // Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: false    // Escapes output if false.
      },
    ];
  }

  getUsers() {
    return axios.get(config.BASE_URL + '/users');
  }

  getMockUsers() {
    return [
      {
        'id': 1,
        'email': 'admin@dr.ru',
        'role': 'ADMIN',
        'name': 'Admin',
        'organization': '',
        'birthDay': '2021-03-20 16:58:51',
        'gender': 'NONE',
        'passportNumber': 'XXXXXXXXXX',
        'city': 'Moscow'
      }
    ];
  }

}

export default new UserService();
