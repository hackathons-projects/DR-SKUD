import axios from 'axios';
import config from '../config';

class TicketService {

  renderAuthMethod(row) {
    let val = '';
    for (const authType of row.authTypes) {
      val += authType + ' ';
    }
    return val;
  }

  renderType(type) {
    switch (type) {
      case 'TEMPORARY':
        return 'Временный';
      case 'REGULAR':
        return 'Постоянный';
      case 'DISPOSABLE':
        return 'Одноразовый';
    }
  }

  renderStatusHtml(status) {
    switch (status) {
      case 'REQUEST':
        return '<div class="mark" style="background: yellow; border-radius: 25px;">' + 'Не подтвержден' + '</div>';
      case 'APPROVED':
        return '<div class="mark" style="background: lightgreen; border-radius: 25px;">' + 'Подтвержден' + '</div>';
      case 'REVOKED':
        return '<div class="mark" style="background: tomato; border-radius: 25px;">' + 'Отозван' + '</div>';
      case 'EXPIRED':
        return '<div class="mark" style="background: lightgrey; border-radius: 25px;">' + 'Истек' + '</div>';
    }
  }

  renderTicketView(row) {
    return '<div>' + this.renderType(row.type) + '</div>' +
      this.renderStatusHtml(row.status);
  }

  getHeaders() {
    return [
      {
        label: 'Id', // Column name
        field: 'id', // Field name from row
        // Use dot for nested props
        // Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: false    // Escapes output if false.
      },
      {
        label: 'Дата выдачи', // Column name
        field: 'activationDate', // Field name from row
        // Use dot for nested props
        // Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: false    // Escapes output if false.
      },
      {
        label: 'Дата завершения', // Column name
        field: 'expirationDate', // Field name from row
// Use dot for nested props
// Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: false    // Escapes output if false.
      },
      {
        label: 'ФИО', // Column name
        field: 'name', // Field name from row
                       // Use dot for nested props
                       // Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: false    // Escapes output if false.
      },
      {
        label: 'Организация', // Column name
        field: 'organization', // Field name from row
        // Use dot for nested props
        // Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: false    // Escapes output if false.
      },
      {
        label: 'Вид пропуска', // Column name
        field: row => this.renderTicketView(row), // Field name from row
// Use dot for nested props
// Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: true    // Escapes output if false.
      },

      {
        label: 'Способ авторизации', // Column name
        field: row => this.renderAuthMethod(row), // Field name from row
// Use dot for nested props
// Can be function with row as 1st param
        numeric: false,// Affects sorting
        html: false    // Escapes output if false.
      },
    ];
  }

  getTickets() {
    return axios.get(config.BASE_URL + '/tickets');
  }

  getMockTickets() {
    return [
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user1@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 1,

        'name': 'User1',
        'phone': '+7..',
        'status': 'REQUEST',
        'type': 'TEMPORARY'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user2@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 2,

        'name': 'User2',
        'phone': '+7..',
        'status': 'APPROVED',
        'type': 'REGULAR'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user3@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 3,

        'name': 'User3',
        'phone': '+7..',
        'status': 'REQUEST',
        'type': 'TEMPORARY'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user4@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 4,

        'name': 'User4',
        'phone': '+7..',
        'status': 'APPROVED',
        'type': 'REGULAR'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user5@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 5,

        'name': 'User5',
        'phone': '+7..',
        'status': 'REQUEST',
        'type': 'TEMPORARY'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user6@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 6,

        'name': 'User6',
        'phone': '+7..',
        'status': 'APPROVED',
        'type': 'REGULAR'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user7@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 7,

        'name': 'User7',
        'phone': '+7..',
        'status': 'REQUEST',
        'type': 'TEMPORARY'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user8@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 8,

        'name': 'User8',
        'phone': '+7..',
        'status': 'APPROVED',
        'type': 'REGULAR'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user9@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 9,

        'name': 'User9',
        'phone': '+7..',
        'status': 'REQUEST',
        'type': 'TEMPORARY'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user10@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 10,

        'name': 'User10',
        'phone': '+7..',
        'status': 'APPROVED',
        'type': 'REGULAR'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user11@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 11,

        'name': 'User11',
        'phone': '+7..',
        'status': 'REQUEST',
        'type': 'TEMPORARY'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user12@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 12,

        'name': 'User12',
        'phone': '+7..',
        'status': 'APPROVED',
        'type': 'REGULAR'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user13@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 13,

        'name': 'User13',
        'phone': '+7..',
        'status': 'REQUEST',
        'type': 'TEMPORARY'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user14@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 14,

        'name': 'User14',
        'phone': '+7..',
        'status': 'APPROVED',
        'type': 'REGULAR'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user15@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 15,

        'name': 'User15',
        'phone': '+7..',
        'status': 'REQUEST',
        'type': 'TEMPORARY'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user16@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 16,

        'name': 'User16',
        'phone': '+7..',
        'status': 'APPROVED',
        'type': 'REGULAR'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user17@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 17,

        'name': 'User17',
        'phone': '+7..',
        'status': 'REQUEST',
        'type': 'TEMPORARY'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user18@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 18,

        'name': 'User18',
        'phone': '+7..',
        'status': 'APPROVED',
        'type': 'REGULAR'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user19@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 19,

        'name': 'User19',
        'phone': '+7..',
        'status': 'REQUEST',
        'type': 'TEMPORARY'
      },
      {
        'activationDate': '20-03-2021',
        'authTypes': [
          'NFC',
          'QR'
        ],
        'checkPointIds': [],
        'email': 'user20@dr.ru',
        'expirationDate': '20-03-2021',
        'id': 20,

        'name': 'User20',
        'phone': '+7..',
        'status': 'APPROVED',
        'type': 'REGULAR'
      }
    ];
  }

}

export default new TicketService();
