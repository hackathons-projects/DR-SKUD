import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  computed: {
    currentUser: function () {
      return 'ADMIN';
    }
  },
  routes: [
    {
      path: '/users',
      name: 'users',
      // lazy-loaded
      component: () => import('./components/Users')
    },
    {
      path: '/organizations',
      name: 'organizations',
      // lazy-loaded
      component: () => import('./components/Organizations')
    },
    {
      path: '/requests',
      name: 'requests',
      // lazy-loaded
      component: () => import('./components/Requests')
    },
    {
      path: '/tickets',
      name: 'tickets',
      // lazy-loaded
      component: () => import('./components/Tickets')
    },
    {
      path: '/history',
      name: 'history',
      // lazy-loaded
      component: () => import('./components/History')
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      // lazy-loaded
      component: () => import('./components/Dashboard')
    },
    {
      path: '/profile',
      name: 'profile',
      // lazy-loaded
      component: () => import('./components/Profile')
    },
    {
      path: '*',
      redirect: '/users'
    }
  ]
});

export default router;
