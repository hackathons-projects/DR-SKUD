package ru.googledie;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import ru.googledie.services.CryptoService;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.UUID;

import static com.google.zxing.client.j2se.MatrixToImageConfig.BLACK;
import static com.google.zxing.client.j2se.MatrixToImageConfig.WHITE;

public class QrTicketActivity extends AppCompatActivity {

    private static final String QR_FORMAT = "PNG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_ticket);

        try {
            CryptoService cryptoService = new CryptoService(this);

            ImageView imageView = findViewById(R.id.imageView);

            String ticket = "TEMPORARY QR" + UUID.randomUUID().toString();

            byte[] bytes = cryptoService.getEncryptAesCipher().doFinal(ticket.getBytes(StandardCharsets.UTF_8));

            Bitmap bitmap = encodeAsBitmap(150, 150, Base64.getEncoder().encodeToString(bytes));
            imageView.setImageBitmap(bitmap);

        } catch (InvalidKeySpecException | NoSuchAlgorithmException | IOException | NoSuchPaddingException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | WriterException e) {
            e.printStackTrace();
        }
        Button cancel = findViewById(R.id.cancelId);
        cancel.setOnClickListener(v -> {
            finish();
        });
        Button send = findViewById(R.id.sendEmailId);
        send.setOnClickListener(v -> {
            AlertDialog alertDialog = new AlertDialog.Builder(QrTicketActivity.this).create();
            alertDialog.setTitle("Ticket to email ");
            alertDialog.setMessage("Отправлено");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    (dialog, which) -> {
                        dialog.dismiss();
                        finish();
                    });
            alertDialog.show();
        });
    }

    Bitmap encodeAsBitmap(int width, int height, String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, width, height, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 150, 0, 0, w, h);
        return bitmap;
    }

    private byte[] generateQRBase64Bytes(String text, int width, int height) throws WriterException, IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
        MatrixToImageWriter.writeToStream(bitMatrix, QR_FORMAT, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
}
