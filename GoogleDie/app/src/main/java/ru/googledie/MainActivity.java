package ru.googledie;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.blikoon.qrcodescanner.QrCodeActivity;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.googledie.model.QrTicketDto;
import ru.googledie.model.TicketDto;
import ru.googledie.services.CryptoService;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Base64;

public class MainActivity extends AppCompatActivity {

    private static final int TICKET_CODE = 33;
    private static final int REQUEST_CODE_QR_SCAN = 101;
    private final String LOGTAG = "QRCScanner-MainActivity";
    private CryptoService cryptoService;

    private static final int CAMERA_REQUEST = 1888;
    private ImageView imageView;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            cryptoService = new CryptoService(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        setContentView(R.layout.activity_main);

        final Button scan = findViewById(R.id.button_start_scan);
        scan.setOnClickListener(v -> {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
            } else {
                //Start the qr scan activity
                Intent i = new Intent(MainActivity.this, QrCodeActivity.class);
                startActivityForResult(i, REQUEST_CODE_QR_SCAN);
            }
        });

        final Button camera = findViewById(R.id.button_start_scan_bio);
        camera.setOnClickListener(v -> {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
            } else {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        final Button qr = findViewById(R.id.button_generate_temporary);
        qr.setOnClickListener(v -> {
            Intent cameraIntent = new Intent(MainActivity.this, QrTicketActivity.class);
            startActivityForResult(cameraIntent, TICKET_CODE);
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != Activity.RESULT_OK) {
            Log.d(LOGTAG, "COULD NOT GET A GOOD RESULT.");
            if (data == null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
            if (result != null) {
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("Scan Error");
                alertDialog.setMessage("QR Code could not be scanned");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        (dialog, which) -> dialog.dismiss());
                alertDialog.show();
            }
            return;

        }
        if (requestCode == REQUEST_CODE_QR_SCAN) {
            if (data == null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
            String dto = null;

            ObjectMapper objectMapper = new ObjectMapper();

            try {
                byte[] bytes = cryptoService.getDecryptAesCipher().doFinal(Base64.getDecoder().decode(result.getBytes(StandardCharsets.UTF_8)));
                result = new String(bytes);

                QrTicketDto qrTicketDto = objectMapper.readValue(result, QrTicketDto.class);
                TicketDto ticketDto = qrTicketDto.ticketDto;
                dto = objectMapper.writeValueAsString(ticketDto);
                String sig = qrTicketDto.signature;

                byte[] ss = Base64.getDecoder().decode(sig);

                Signature signatureForVerify = cryptoService.getSignatureForVerify();
                signatureForVerify.update(dto.getBytes(StandardCharsets.UTF_8));
                boolean verify = signatureForVerify.verify(ss);
                System.out.println(verify);
                System.out.println();
                result = verify ? "SUCCESS" : "FAILURE";
            } catch (BadPaddingException | IllegalBlockSizeException | IOException | SignatureException e) {
                e.printStackTrace();
            }

            Log.d(LOGTAG, "Have scan result in your app activity :" + result);
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle("Scan result = " + result);
            alertDialog.setMessage(dto);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    (dialog, which) -> dialog.dismiss());
            alertDialog.show();

        }
        if (requestCode == CAMERA_REQUEST) {
//            TODO SEND TO SERVER
//            Bitmap photo = (Bitmap) data.getExtras().get("data");
//            imageView.setImageBitmap(photo);

            Log.d(LOGTAG, "Have scan result in your app activity");
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle("Scan biometry result");
            alertDialog.setMessage("PERSON FAILED");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    (dialog, which) -> dialog.dismiss());
            alertDialog.show();
        }
    }

}
